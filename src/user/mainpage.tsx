import React, { useCallback, useEffect } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import AddIcon from "@material-ui/icons/Add";
import IconButton from "@material-ui/core/IconButton";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { TicketAdd } from "./addticket";
import { useStyles } from "../style/style";
import Grid from "@material-ui/core/Grid";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { TicketEdit } from "./editticket";
import { AppState } from "../components/types";
import { Fab, Tooltip } from "@material-ui/core";

type TicketType = {
  _id: number;
  id: String;
  priority: String;
  title: String;
  description: String;
  assigned: String;
  date: String;
};

type CellProps = {
  row: TicketType;
};
export function MainPage() {
  const classes = useStyles();
  const [rows, setRows] = React.useState<TicketType[]>([]);
  const [open, setOpen] = React.useState(false);
  const [openedit, setOpenEdit] = React.useState(false);
  const [row, setRow] = React.useState<TicketType>();
  const tokens = useSelector((state: AppState) => state.tokens);
  const loginToken = tokens.length === 0 ? "" : tokens[0].loginToken;
  const users = useSelector((state: AppState) => state.users);
  const user = users[0];

  const history = useHistory();
  const listTickets = useCallback(() => {
      fetch("http://localhost:9000/tickets", {
        method: "GET",
        headers: {
          Authorization: "Bearer " + loginToken,
        },
      })
        .then((res) => res.json())
        .then((res) => {
          if (res === "Acces denied") {
            history.replace("/login");
          } else {
            setRows(res);
          }
        });
  }, [loginToken, history]);

  useEffect(() => {
    listTickets();
  }, [listTickets]);

  const openDialog = () => {
    setOpen(true);
  };

  const closeDialog = () => {
    setOpen(false);
  };

  const openEditDialog = (rowParam: TicketType) => {
    setRow(rowParam);
    setOpenEdit(true);
  };

  const closeEditDialog = () => {
    setOpenEdit(false);
    setRow(undefined);
  };

  const assignMyself = (value: String, id: String) => {
    if (value === "Unassigned") {
      fetch("http://localhost:9000/tickets/" + id, {
        method: "PATCH",
        headers: {
          "Content-type": "application/json",
          Authorization: "Bearer " + loginToken,
        },
        body: JSON.stringify({
          assigned: user.firstName + " " + user.lastName,
        }),
      })
        .then((result) => result.json())
        .then((info) => {
          listTickets();
        });
    }
  };

  const AssignedCell = (props: CellProps) => {
    const { row } = props;
    if (row.assigned === "Unassigned")
      return (
        <Tooltip arrow title="Click to assign yourself">
          <TableCell
            align="center"
            onClick={() => assignMyself(row.assigned, row.id)}
            className={classes.assigned}
          >
            {row.assigned}
          </TableCell>
        </Tooltip>
      );
    return (
      <TableCell
        align="center"
        onClick={() => assignMyself(row.assigned, row.id)}
        className={classes.assigned}
      >
        {row.assigned}
      </TableCell>
    );
  };

  const deleteTicket = (id: String) => {
    fetch("http://localhost:9000/tickets/" + id, {
      method: "DELETE",
      headers: {
        "Content-type": "application/json",
        Authorization: "Bearer " + loginToken,
      },
    })
      .then((result) => result.json())
      .then((info) => {
        listTickets();
      });
  };

  const editDelete = (rowParam: TicketType) => {
    return (
      <>
        <IconButton size="small" onClick={() => openEditDialog(rowParam)}>
          <EditIcon color="primary" />
        </IconButton>
        <IconButton size="small" onClick={() => deleteTicket(rowParam.id)}>
          <DeleteIcon className={classes.delete} />
        </IconButton>
      </>
    );
  };
  return (
    <>
      <TableContainer>
        <Grid container className={classes.grid}>
          <Table size="small" className={classes.grid}>
            <TableBody>
              {rows.map((row: TicketType) => (
                <TableRow key={row._id}>
                  <TableCell
                    align="center"
                    className={
                      row.priority === "High"
                        ? classes.highprio
                        : row.priority === "Low"
                        ? classes.lowprio
                        : classes.closedprio
                    }
                  >
                    {row.priority}
                  </TableCell>
                  <TableCell align="left" className={classes.titleTable}>
                    {row.title}
                  </TableCell>
                  <TableCell align="left" className={classes.description}>
                    {row.description}
                  </TableCell>
                  <AssignedCell row={row} />
                  <TableCell align="right" className={classes.date}>
                    {row.date}
                  </TableCell>
                  <TableCell align="center" className={classes.editdeletecell}>
                    {editDelete(row)}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Grid>
      </TableContainer>
      <Fab color="primary" className={classes.addbutton} onClick={openDialog}>
        <AddIcon className={classes.addbuttonIcon} fontSize="large" />
      </Fab>

      <Dialog open={open} onClose={closeDialog}>
        <DialogTitle>New ticket</DialogTitle>
        <DialogContent>
          <TicketAdd closeDialog={closeDialog} listTickets={listTickets} />
        </DialogContent>
      </Dialog>

      <Dialog open={openedit} onClose={closeEditDialog}>
        <DialogTitle>Edit ticket</DialogTitle>
        <DialogContent>
          <TicketEdit
            closeDialog={closeEditDialog}
            listTickets={listTickets}
            row={row!}
          />
        </DialogContent>
      </Dialog>
    </>
  );
}
