import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import { MainPage } from "./user/mainpage";
import { AdminPage } from "./admin/adminpage";
import { Password } from "./login/password";
import { BrowserRouter, Route, useHistory, Switch } from "react-router-dom";
import CssBaseline from "@material-ui/core/CssBaseline";
import { TabPanel } from "./components/TabPanel";
import { Login } from "./login/login";
import { Provider, useSelector } from "react-redux";
import { PersistGate } from "redux-persist/lib/integration/react";
import { persistor, store } from "./reducer/persist";
import { TopBar } from "./components/AppBar";
import { AppState } from "./components/types";

function App() {
  const [activeTab, setActiveTab] = React.useState(1);
  const handleChangeTab = (newTab: number) => {
    setActiveTab(newTab);
  };
  const timeRedux = useSelector((state: AppState) => state.time);
  const time = timeRedux.length === 0 ? 0 : timeRedux[0].refreshTime;
  const tokens = useSelector((state: AppState) => state.tokens);
  const refreshToken = tokens.length === 0 ? "" : tokens[0].refreshToken;
  const history = useHistory();

  useEffect(() => {
    if (window.location.pathname.split("/")[1] !== "register") {
      fetch("http://localhost:9000/login", {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify({
          refreshToken: refreshToken,
        }),
      })
        .then((result) => result.json())
        .then((info) => {
          if (info.error === "Refresh token not valid!") {
            history.push("/login");
          } else {
            store.dispatch({
              type: "EDIT_TOKEN",
              payload: {
                loginToken: info.token,
                refreshToken: info.refreshToken,
              },
            });
            store.dispatch({
              type: "EDIT_TIME",
              payload: {
                refreshTime: new Date().getTime(),
              },
            });
            history.push("/");
          }
        });
    }
  }, [refreshToken, history]);

  useEffect(() => {
    if (window.location.pathname.split("/")[1] !== "register") {
      const timer = setTimeout(() => {
        fetch("http://localhost:9000/login", {
          method: "POST",
          headers: {
            "Content-type": "application/json",
          },
          body: JSON.stringify({
            refreshToken: refreshToken,
          }),
        })
          .then((result) => result.json())
          .then((info) => {
            if (info.error === "Refresh token not valid!") {
              history.push("/login");
            } else {
              store.dispatch({
                type: "EDIT_TOKEN",
                payload: {
                  loginToken: info.token,
                  refreshToken: info.refreshToken,
                },
              });
              store.dispatch({
                type: "EDIT_TIME",
                payload: {
                  refreshTime: new Date().getTime(),
                },
              });
            }
          });
      }, 3540000 - (new Date().getTime() - time));
      return () => clearTimeout(timer);
    }
  }, [time, refreshToken, history]);

  return (
    <Switch>
      <Route
        exact={true}
        path="/"
        render={() => (
          <>
            <CssBaseline />
            <TopBar handleChangeTab={handleChangeTab} />
            <TabPanel value={activeTab} index={1}>
              <MainPage />
            </TabPanel>
            <TabPanel value={activeTab} index={2}>
              <AdminPage />
            </TabPanel>
          </>
        )}
      />
      <Route exact={true} path="/register/:tokens" component={Password} />
      <Route exact={true} path="/login" component={Login} />
    </Switch>
  );
}

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </PersistGate>
  </Provider>,
  document.getElementById("maindiv")
);
