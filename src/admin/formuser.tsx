import React from "react";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import CheckBox from "@material-ui/core/Checkbox";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Paper from "@material-ui/core/Paper";
import { useStyles } from "../style/style";
import SaveIcon from "@material-ui/icons/Save";
import Button from "@material-ui/core/Button";
import { AppState } from "../components/types";
import { useSelector } from "react-redux";

type UserType = {
  _id: Number;
  id: String;
  firstname: String;
  lastname: String;
  email: String;
  role: String;
  enabled: boolean;
};
type UserDataProps = {
  user: UserType;
  listUsers: Function;
};

type Sendable = {
  firstname?: String;
  lastname?: String;
  email?: String;
  role?: String;
  enabled?: boolean;
};

export function UserData(props: UserDataProps) {
  const { user, listUsers } = props;
  const classes = useStyles();
  const tokens = useSelector((state: AppState) => state.tokens);
  const loginToken = tokens.length === 0 ? "" : tokens[0].loginToken;
  const [firstname, setFirstname] = React.useState(user.firstname);
  const [lastname, setLastname] = React.useState(user.lastname);
  const [email, setEmail] = React.useState(user.email);
  const [role, setRole] = React.useState(user.role);
  const [enabled, setEnabled] = React.useState(user.enabled);

  const edit = () => {
    const send: Sendable = {};
    if (firstname !== user.firstname) {
      send.firstname = firstname;
    }
    if (lastname !== user.lastname) {
      send.lastname = lastname;
    }
    if (email !== user.email) {
      send.email = email;
    }
    if (role !== user.role) {
      send.role = role;
    }
    if (enabled !== user.enabled) {
      send.enabled = enabled;
    }
    const sendJSON = JSON.stringify(send);
    fetch("http://localhost:9000/users/" + user.id, {
      method: "PATCH",
      headers: {
        "Content-type": "application/json",
        Authorization: "Bearer " + loginToken,
      },
      body: sendJSON,
    })
      .then((result) => result.json())
      .then((info) => {
        listUsers();
      });
  };
  return (
    <Paper className={classes.pageContent}>
      <FormControl className={classes.rootUser}>
        <TextField
          defaultValue={firstname}
          variant="outlined"
          size="small"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            setFirstname(e.target.value);
          }}
        />

        <TextField
          defaultValue={lastname}
          variant="outlined"
          size="small"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            setLastname(e.target.value);
          }}
        />

        <TextField
          defaultValue={email}
          variant="outlined"
          size="small"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            setEmail(e.target.value);
          }}
        />
        <Select
          className={classes.select}
          defaultValue={role}
          label={role}
          onChange={(event: React.ChangeEvent<{ value: unknown }>) => {
            setRole(event.target.value as string);
          }}
        >
          <MenuItem value={"admin"}>admin</MenuItem>
          <MenuItem value={"user"}>user</MenuItem>
        </Select>
        <FormControlLabel
          control={
            <CheckBox
              className={classes.checkbox}
              defaultChecked={enabled}
              onChange={() => setEnabled(!enabled)}
            />
          }
          label="Enabled"
        />
        <Button
          variant="contained"
          color="primary"
          size="large"
          className={classes.button}
          startIcon={<SaveIcon />}
          onClick={edit}
        >
          Save
        </Button>
      </FormControl>
    </Paper>
  );
}
