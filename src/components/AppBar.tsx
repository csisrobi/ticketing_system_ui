import React, { useEffect } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import Button from "@material-ui/core/Button";
import { useStyles } from "../style/style";
import { store } from "../reducer/persist";
import { AppState } from "./types";

type TopBarProps = {
  handleChangeTab: Function;
};

export function TopBar(props: TopBarProps) {
  const { handleChangeTab } = props;
  const classes = useStyles();
  const [activeTab, setActiveTab] = React.useState(1);
  const [role, setRole] = React.useState(false);
  const [username, setUsername] = React.useState("");
  const tokens = useSelector((state: AppState) => state.tokens);
  const loginToken = tokens.length === 0 ? "" : tokens[0].loginToken;
  const refreshToken = tokens.length === 0 ? "" : tokens[0].refreshToken;
  const history = useHistory();

  const handleChange = (event: React.ChangeEvent<{}>, newTab: number) => {
    setActiveTab(newTab);
    handleChangeTab(newTab);
  };

  const TitleTyp = () => {
    return (
      <Typography variant="h5" className={classes.title}>
        Ticketing System
      </Typography>
    );
  };

  const LogoutButton = () => {
    return (
      <Button
        size="small"
        variant="contained"
        onClick={logout}
        className={username !== "" ? classes.showbutton : classes.hide}
      >
        Logout
      </Button>
    );
  };

  const Name = () => {
    return (
      <Typography className={username !== "" ? classes.welcome : classes.hide}>
        Welcome: {username}
      </Typography>
    );
  };

  const logout = () => {
    store.dispatch({
      type: "LOGOUT",
    });

    fetch("http://localhost:9000/logout", {
      method: "DELETE",
      headers: {
        "Content-type": "application/json",
        Authorization: "Bearer " + loginToken,
      },
      body: JSON.stringify({
        refreshToken: refreshToken,
      }),
    })
      .then((result) => result.json())
      .then((info) => {
        setActiveTab(1);
        handleChangeTab(1);
        history.push("/login");
      });
  };

  useEffect(() => {
      fetch("http://localhost:9000/profile", {
        method: "GET",
        headers: {
          Authorization: "Bearer " + loginToken,
        },
      })
        .then((res) => res.json())
        .then((info) => {
          if (info === "Acces denied") {
            history.replace("/login");
          }
          if (info.role === "admin") {
            setRole(true);
          } else {
            setRole(false);
          }
          store.dispatch({
            type: "ADD_USER",
            payload: {
              firstName: info.firstname,
              lastName: info.lastname,
              role: info.role,
            },
          });
          const newUsername = info.firstname + " " + info.lastname;
          setUsername(newUsername);
        });
  }, [loginToken, history]);

  return (
    <AppBar position="fixed">
      <Tabs value={activeTab} onChange={handleChange}>
        <TitleTyp />
        <Tab
          label="Tickets"
          className={role === true ? classes.showtab : classes.hide}
        />
        <Tab
          label="Users"
          className={role === true ? classes.showtab : classes.hide}
        />
        <LogoutButton />
        <Name />
      </Tabs>
    </AppBar>
  );
}
