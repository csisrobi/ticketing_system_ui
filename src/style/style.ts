import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      paddingTop: theme.spacing(1),
      marginLeft: theme.spacing(2),
    },
    hide: {
      display: "none",
    },
    showtab: {
      display: "block",
    },
    showlabelPassword: {
      display: "block",
      color: "red",
    },
    rootadmin: {
      flexGrow: 1,
      display: "flex",
      height: "100%",
      paddingTop: theme.spacing(6),
    },
    tabs: {
      borderRight: `1px solid ${theme.palette.divider}`,
    },
    tab: {
      "&  .MuiTab-wrapper": {
        alignItems: "start",
      },
    },
    addbutton: {
      height: 60,
      width: 60,
      position: "fixed",
      bottom: theme.spacing(5),
      right: theme.spacing(5),
      color: "red",
    },
    addbuttonIcon: {
      height: 60,
      width: 60,
    },
    rootUser: {
      "&  .MuiFormControl-root": {
        width: 400,
        margin: theme.spacing(1),
      },
    },
    pageContent: {
      width: 500,
      height: 790,
      padding: theme.spacing(3),
    },
    select: {
      width: 400,
      marginLeft: 10,
    },
    checkbox: {
      marginLeft: 10,
    },
    save: {
      marginLeft: theme.spacing(40),
    },
    button: {
      width: theme.spacing(30),
      marginLeft: theme.spacing(12),
      marginTop: theme.spacing(2),
    },
    tableContainer: {
      height: 330,
      overflow: "auto",
    },
    highprio: {
      background: "#e06666",
      border: "1px solid #d8d8d8",
      width: 180,
    },
    lowprio: {
      background: "#ffd966",
      border: "1px solid #d8d8d8",
      width: 180,
    },
    closedprio: {
      background: "#cccccc",
      border: "1px solid #d8d8d8",
      width: 180,
    },
    date: {
      border: "1px solid #d8d8d8",
      width: 180,
    },
    assigned: {
      border: "1px solid #d8d8d8",
      width: 200,
      cursor: "pointer",
    },
    titleTable: {
      border: "1px solid #d8d8d8",
      width: 250,
    },
    description: {
      border: "1px solid #d8d8d8",
      overflow: "hidden",
      textOverflow: "ellipsis",
    },
    passroot: {
      marginLeft: theme.spacing(3),
    },
    grid: {
      maxWidth: "100%",
      maxHeight: "100%",
      paddingTop: theme.spacing(6),
    },
    loginPaper: {
      width: 500,
      height: 250,
      padding: theme.spacing(3, 2),
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
    },
    loginGrid: {
      minHeight: "100%",
    },
    welcome: {
      fontSize: "medium",
      position: "fixed",
      top: theme.spacing(1.5),
      right: theme.spacing(1),
    },
    showbutton: {
      display: "block",
      position: "fixed",
      top: theme.spacing(1),
      right: theme.spacing(22),
    },
    paperTitle: {
      marginLeft: theme.spacing(12),
      paddingBottom: theme.spacing(2),
    },
    delete: {
      color: "red",
    },
    editdeletecell: {
      border: "1px solid #d8d8d8",
      width: 100,
    },
    registerPaper: {
      width: theme.spacing(40),
      padding: theme.spacing(3, 5),
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
    },
    registerButton: {
      marginTop: theme.spacing(3),
    },
    forgotpass: {
      paddingLeft: theme.spacing(2),
      cursor: "pointer"
    },
    newToken:{
      paddingBottom:theme.spacing(1),
      width:theme.spacing(45)
    }
  })
);
