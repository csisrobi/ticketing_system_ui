import React, { useCallback, useEffect } from 'react'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import { UserData } from './formuser';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { UserAdd } from './adduser'
import { TabPanel } from '../components/TabPanel'
import { useStyles } from '../style/style'
import { useSelector } from 'react-redux';
import { AppState } from '../components/types';
import Fab from '@material-ui/core/Fab';
import AddIcon from "@material-ui/icons/Add";

type UserType = {
  _id: number,
  id: string,
  firstname: String,
  lastname: String,
  email: String,
  role: String,
  enabled: boolean
}


export function AdminPage() {
  const classes = useStyles();
  const [activeTab, setActiveTab] = React.useState(0);
  const [users, setUsers] = React.useState<UserType[]>([]);
  const [open, setOpen] = React.useState(false);
  const tokens = useSelector((state: AppState) => state.tokens);
  const loginToken = (tokens.length === 0 ? '' : tokens[0].loginToken);
  const handleChange = (event: React.ChangeEvent<{}>, newTab: number) => {
    setActiveTab(newTab);
  }

  const listUsers = useCallback(() => {
    fetch('http://localhost:9000/users', {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + loginToken
      }
    })
      .then(res => res.json())
      .then(res => {
        setUsers(res);
      });
  }, [loginToken])

  const openDialog = () => {
    setOpen(true);
  }

  const closeDialog = () => {
    setOpen(false);
  }

  useEffect(() => {
    listUsers()
  }, [listUsers])

  return (
    <>
      <div className={classes.rootadmin}>
        <Tabs className={classes.tabs}
          value={activeTab}
          onChange={handleChange}
          orientation="vertical"
          variant="scrollable">
          {users.map((item: UserType) => {
            return <Tab key={item.id} className={classes.tab} label={item.firstname + ' ' + item.lastname} />
          })}
        </Tabs>
        {users.map((item: UserType, index: number) => {
          return <TabPanel key={item.id} value={activeTab} index={index}><UserData user={item} listUsers={listUsers} />
          </TabPanel>
        })}
      </div>

      <Fab color='primary' className={classes.addbutton} onClick={openDialog}>
        <AddIcon
          className={classes.addbuttonIcon}
          fontSize="large"
        />
      </Fab>

      <Dialog open={open} onClose={closeDialog}>

        <DialogTitle>New user</DialogTitle>
        <DialogContent>
          <UserAdd closeDialog={closeDialog} listUsers={listUsers} />
        </DialogContent>
      </Dialog>

    </>

  );

}
