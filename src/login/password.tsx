import React from "react";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import FiberNewOutlinedIcon from "@material-ui/icons/FiberNewOutlined";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import { useHistory, RouteComponentProps } from "react-router-dom";
import { useStyles } from "../style/style";
import Button from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";
import Paper from "@material-ui/core/Paper";

type NewTokenProps = {
  closeDialog: Function;
};

type PassParams = { tokens: string };

function NewToken(props: NewTokenProps) {
  const [email, setEmail] = React.useState("");
  const classes = useStyles();
  const token = () => {
    if (email !== "") {
      fetch("http://localhost:9000/registration", {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify({
          email: email,
        }),
      })
        .then((result) => result.json())
        .then((info) => {
          props.closeDialog();
        });
    }
  };
  return (
    <FormControl>
      <TextField
        variant="outlined"
        size="small"
        label="Email"
        value={email}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
          setEmail(e.target.value)
        }
        className={classes.newToken}
      />
      <Button variant="contained" color="primary" onClick={token}>
        SEND
      </Button>
    </FormControl>
  );
}

export function Password({ match }: RouteComponentProps<PassParams>) {
  const history = useHistory();
  const classes = useStyles();
  const [password, setPassword] = React.useState("");
  const [passagain, setPassagain] = React.useState("");
  const [notvalabil, setNotvalabil] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const openDialog = () => {
    setOpen(true);
  };

  const closeDialog = () => {
    setOpen(false);
  };

  const loginPage = () => {
    if (passagain !== password && password === "") {
      return;
    }
    fetch("http://localhost:9000/registration", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        password: password,
        passwordConfirm: passagain,
        token: match.params.tokens,
      }),
    })
      .then((result) => result.json())
      .then((info) => {
        if (info === "Password changed") {
          history.replace("/login");
        } else {
          setNotvalabil(true);
        }
      });
  };

  return (
    <Paper className={classes.registerPaper}>
      <FormControl className={classes.passroot}>
        <TextField
          type="password"
          error={passagain !== password ? true : false}
          label="Password"
          value={password}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            setPassword(e.target.value)
          }
        />

        <TextField
          type="password"
          error={passagain !== password ? true : false}
          helperText={passagain !== password ? "Password does not match" : ""}
          label="Password again"
          value={passagain}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            setPassagain(e.target.value)
          }
        />

        <Button
          variant="contained"
          color="primary"
          size="large"
          onClick={() => {
            loginPage();
          }}
          className={classes.registerButton}
          startIcon={<SaveIcon />}
        >
          Save
        </Button>

        <Button
          variant="contained"
          color="secondary"
          onClick={openDialog}
          className={
            notvalabil === false ? classes.hide : classes.showlabelPassword
          }
          startIcon={<FiberNewOutlinedIcon />}
        >
          Token not valid!
        </Button>
      </FormControl>
      <Dialog open={open} onClose={closeDialog}>
        <DialogTitle>Where to send the new token</DialogTitle>
        <DialogContent>
          <NewToken closeDialog={closeDialog} />
        </DialogContent>
      </Dialog>
    </Paper>
  );
}
