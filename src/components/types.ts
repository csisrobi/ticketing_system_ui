export type Token = {
  loginToken: string,
  refreshToken: string
}

export type User = {
  firstName: string,
  lastName: string,
  role: string
}

export type Time = {
  refreshTime: number
}

export type AppState = {
  time: Time[],
  users: User[],
  tokens: Token[]
}
