import React from 'react'

type TabPanelProps = {
  children: React.ReactNode;
  index: number;
  value: number;
}

export function TabPanel(props: TabPanelProps) {
  const { children, value, index } = props;

  return (
    <div hidden={value !== index}>
      {value === index && (
        <span>{children}</span>
      )}
    </div>
  );

}
