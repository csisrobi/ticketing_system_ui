import React from "react";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import { useStyles } from "../style/style";
import Button from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";
import CloseIcon from "@material-ui/icons/Close";
import { useSelector } from "react-redux";
import { AppState } from "../components/types";

type UserAddProps = {
  closeDialog: Function;
  listTickets: Function;
};

export function TicketAdd(props: UserAddProps) {
  const { closeDialog, listTickets } = props;
  const classes = useStyles();
  const [priority, setPriority] = React.useState("");
  const [title, setTitle] = React.useState("");
  const [description, setDescription] = React.useState("");
  const [blank, setBlank] = React.useState(false);
  const tokens = useSelector((state: AppState) => state.tokens);
  const loginToken = tokens[0].loginToken;

  const add = () => {
    if (priority !== "" && title !== "" && description !== "") {
      fetch("http://localhost:9000/tickets", {
        method: "POST",
        headers: {
          "Content-type": "application/json",
          Authorization: "Bearer " + loginToken,
        },
        body: JSON.stringify({
          priority: priority,
          title: title,
          description: description,
        }),
      })
        .then((result) => result.json())
        .then((info) => {
          listTickets();
          setTitle("");
          setPriority("");
          setDescription("");
          setBlank(false);
          closeDialog();
        });
    } else {
      setBlank(true);
    }
  };
  return (
    <FormControl className={classes.rootUser}>
      <TextField
        value={title}
        label="Title"
        variant="outlined"
        size="small"
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          setTitle(e.target.value);
        }}
        error={blank}
        helperText={blank === true ? "No blank field allowed" : ""}
      />
      <FormControl>
        <InputLabel id="prioritylabel">Priority</InputLabel>
        <Select
          labelId="prioritylabel"
          value={priority}
          onChange={(event: React.ChangeEvent<{ value: unknown }>) =>
            setPriority(event.target.value as string)
          }
        >
          <MenuItem value={"High"}>High</MenuItem>
          <MenuItem value={"Low"}>Low</MenuItem>
          <MenuItem value={"Closed"}>Closed</MenuItem>
        </Select>
      </FormControl>
      <TextField
        multiline
        rows={5}
        value={description}
        label="Description"
        variant="outlined"
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          setDescription(e.target.value);
        }}
        error={blank}
        helperText={blank === true ? "No blank field allowed" : ""}
      />

      <Button
        variant="contained"
        color="primary"
        size="large"
        className={classes.button}
        onClick={() => {add();
        }}
        startIcon={<SaveIcon />}
      >
        Save
      </Button>
      <Button
        variant="contained"
        color="primary"
        size="large"
        className={classes.button}
        onClick={() => {
          closeDialog();
        }}
        startIcon={<CloseIcon />}
      >
        Close
      </Button>
    </FormControl>
  );
}
