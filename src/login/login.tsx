import React, { useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import { useHistory } from "react-router-dom";
import { useStyles } from "../style/style";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import {
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  Typography,
} from "@material-ui/core";
import { useDispatch } from "react-redux";

type NewTokenProps = {
  closeDialog: Function;
};

function NewToken(props: NewTokenProps) {
  const [email, setEmail] = React.useState("");
  const classes = useStyles();
  const token = () => {
    if (email !== "") {
      fetch("http://localhost:9000/registration", {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify({
          email: email,
        }),
      })
        .then((result) => result.json())
        .then((info) => {
          props.closeDialog();
        });
    }
  };
  return (
    <FormControl>
      <TextField
        variant="outlined"
        size="small"
        label="Email"
        value={email}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
          setEmail(e.target.value)
        }
        className={classes.newToken}
      />
      <Button variant="contained" color="primary" onClick={token}>
        SEND
      </Button>
    </FormControl>
  );
}

export function Login() {
  const dispatch = useDispatch();
  const history = useHistory();
  const classes = useStyles();
  const [password, setPassword] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [emailInvalid, setEmailinvalid] = React.useState(false);
  const [passInvalid, setPassinvalid] = React.useState(false);
  const [disabled, setDisabled] = React.useState(false);
  const [open, setOpen] = React.useState(false);

  const openDialog = () => {
    setOpen(true);
  };

  const closeDialog = () => {
    setOpen(false);
  };
  const sendLogin = () => {
    fetch("http://localhost:9000/login", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        pass: password,
        email: email,
      }),
    })
      .then((result) => result.json())
      .then((info) => {
        if (info.error === "Invalid email") {
          setEmailinvalid(true);
        } else if (info.error === "Invalid password") {
          setEmailinvalid(false);
          setPassinvalid(true);
        } else if (info.error === "User disabled") {
          setDisabled(true);
        } else {
          setEmailinvalid(false);
          setPassinvalid(false);
          setDisabled(false);
          dispatch({
            type: "EDIT_TOKEN",
            payload: {
              loginToken: info.token,
              refreshToken: info.refreshToken,
            },
          });
          dispatch({
            type: "EDIT_TIME",
            payload: {
              refreshTime: new Date().getTime(),
            },
          });
          history.replace("/");
        }
      });
  };

  useEffect(() => {
    fetch("http://localhost:9000/admin", {
      method: "GET",
      headers: {
        "Content-type": "application/json",
      },
    });
  }, []);

  return (
    <Grid
      container
      direction="row"
      justify="center"
      alignContent="center"
      className={classes.loginGrid}
    >
      <Paper elevation={7} className={classes.loginPaper}>
        <Typography variant="h5" className={classes.paperTitle}>
          Ticketing-system login
        </Typography>
        <FormControl className={classes.rootUser}>
          <TextField
            label="Email"
            value={email}
            variant="outlined"
            size="small"
            error={emailInvalid || disabled}
            helperText={
              emailInvalid === true
                ? "Invalid email!"
                : disabled === true
                ? "User disabled"
                : ""
            }
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              setEmail(e.target.value)
            }
          />

          <TextField
            type="Password"
            label="Password"
            variant="outlined"
            size="small"
            value={password}
            error={passInvalid}
            helperText={
              passInvalid === false ? "" : "Password and email doesn't match"
            }
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              setPassword(e.target.value)
            }
          />
          <Typography
            className={classes.forgotpass}
            color="primary"
            variant="body2"
            onClick={() => openDialog()}
          >
            Forgot your password?
          </Typography>
          <Button
            className={classes.button}
            variant="contained"
            color="primary"
            onClick={sendLogin}
          >
            LOGIN
          </Button>
        </FormControl>
      </Paper>
      <Dialog open={open} onClose={closeDialog}>
        <DialogTitle>Where to send the email</DialogTitle>
        <DialogContent>
          <NewToken closeDialog={closeDialog} />
        </DialogContent>
      </Dialog>
    </Grid>
  );
}
