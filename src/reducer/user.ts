import {User,Token,Time, AppState } from '../components/types'
const ADD_USER = 'ADD_USER';
const LOGOUT = 'LOGOUT';
const ADD_TOKEN = 'ADD_TOKEN';
const EDIT_TOKEN = 'EDIT_TOKEN';
const EDIT_TIME = 'EDIT_TIME';


type AddActionType = {
  type: typeof ADD_USER,
  payload: User,
}

type RemoveActionType = {
  type: typeof LOGOUT
}

type AddTokenActionType = {
  type: typeof ADD_TOKEN,
  payload: Token
}

type EditTokenActionType = {
  type: typeof EDIT_TOKEN,
  payload: Token
}

type EditTimeActionType = {
  type: typeof EDIT_TIME,
  payload: Time
}


const initialState: AppState = {
  time: [],
  users: [],
  tokens: []
};

type Action = AddActionType | RemoveActionType | AddTokenActionType | EditTokenActionType | EditTimeActionType;

export const usersreducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case 'ADD_USER':
      const newUser = action.payload;
      return {
        time: [...state.time],
        users: [
          newUser
        ],
        tokens: [...state.tokens]
      }

    case 'EDIT_TOKEN':
      const editToken = action.payload;
      return {
        time: [...state.time],
        users: [...state.users],
        tokens: [
          editToken
        ]
      }

    case 'EDIT_TIME':
      const edittime = action.payload;
      return {
        time: [edittime],
        users: [...state.users],
        tokens: [...state.tokens]
      }

    case 'LOGOUT':
      return { time: [], users: [], tokens: [] };

    default:
      return state
  }
}
