import React from 'react'
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import { useStyles } from '../style/style';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import CloseIcon from '@material-ui/icons/Close';
import { useSelector } from 'react-redux';
import { AppState } from '../components/types'

type TicketType = {
  _id: number,
  id: String,
  priority: String,
  title: String,
  description: String,
  assigned: String,
  date: String
}

type UserAddProps = {
  closeDialog: Function,
  listTickets: Function,
  row: TicketType

}

type Sendable = {
  priority?: String,
  description?: String,
  title?: String
}

export function TicketEdit(props: UserAddProps) {
  const { closeDialog, listTickets, row } = props;
  const classes = useStyles();
  const [priority, setPriority] = React.useState(row === undefined ? '' : row.priority);
  const [title, setTitle] = React.useState(row === undefined ? '' : row.title);
  const [description, setDescription] = React.useState(row === undefined ? '' : row.description);
  const tokens = useSelector((state: AppState) => state.tokens);
  const loginToken = tokens[0].loginToken;

  const add = () => {
    const send: Sendable = {}
    if (priority !== row.priority) {
      send.priority = priority;
    }
    if (description !== row.description) {
      send.description = description;
    }
    if (title !== row.title) {
      send.title = title;
    }
    const sendJSON = JSON.stringify(send);
    fetch('http://localhost:9000/tickets/' + row.id, {
      method: 'PATCH',
      headers: {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + loginToken
      },
      body: sendJSON,
    })
      .then((result) => result.json())
      .then((info) => {
        listTickets();
      })

  }
  return (
    <FormControl className={classes.rootUser} >
      <TextField value={title}
        label="Title"
        variant="outlined"
        size="small"
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => { setTitle(e.target.value); }} />
      <FormControl>
        <InputLabel id="prioritylabel">Priority</InputLabel>
        <Select labelId="prioritylabel"
          value={priority}
          onChange={(event: React.ChangeEvent<{ value: unknown }>) => { setPriority(event.target.value as string); }}>
          <MenuItem value={'High'}>High</MenuItem>
          <MenuItem value={'Low'}>Low</MenuItem>
          <MenuItem value={'Closed'}>Closed</MenuItem>
        </Select>
      </FormControl>
      <TextField
        multiline
        rows={5}
        value={description}
        label="Description"
        variant="outlined"
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => { setDescription(e.target.value); }}
      />

      <Button
        variant="contained"
        color="primary"
        size="large"
        className={classes.button}
        onClick={() => { add(); closeDialog() }}
        startIcon={<SaveIcon />}
      >
        Save
      </Button>
      <Button
        variant="contained"
        color="primary"
        size="large"
        className={classes.button}
        onClick={() => { closeDialog() }}
        startIcon={<CloseIcon />}
      >
        Close
      </Button>
    </FormControl>
  )
}
