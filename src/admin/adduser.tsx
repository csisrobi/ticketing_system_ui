import React from "react";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import CheckBox from "@material-ui/core/Checkbox";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import InputLabel from "@material-ui/core/InputLabel";
import { useStyles } from "../style/style";
import Button from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";
import CloseIcon from "@material-ui/icons/Close";
import { useSelector } from "react-redux";
import { AppState } from "../components/types";

type UserAddProps = {
  closeDialog: Function;
  listUsers: Function;
};

export function UserAdd(props: UserAddProps) {
  const { closeDialog, listUsers } = props;
  const classes = useStyles();
  const [firstname, setFirstName] = React.useState("");
  const [lastname, setLastName] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [role, setRole] = React.useState("");
  const [enabled, setEnabled] = React.useState(true);
  const [blank, setBlank] = React.useState(false);
  const tokens = useSelector((state: AppState) => state.tokens);
  const loginToken = tokens.length === 0 ? "" : tokens[0].loginToken;

  const add = () => {
    if (firstname !== "" && lastname !== "" && email !== "" && role !== "") {
      fetch("http://localhost:9000/users", {
        method: "POST",
        headers: {
          "Content-type": "application/json",
          Authorization: "Bearer " + loginToken,
        },
        body: JSON.stringify({
          firstname: firstname,
          lastname: lastname,
          email: email,
          role: role,
          enabled: enabled,
        }),
      })
        .then((result) => result.json())
        .then((info) => {
          listUsers();
          setFirstName("");
          setLastName("");
          setEmail("");
          setRole("");
          setEnabled(true);
          setBlank(false);
          closeDialog();
        });
    } else {
      setBlank(true);
    }
  };
  return (
    <FormControl className={classes.rootUser}>
      <TextField
        value={firstname}
        label="First name"
        variant="outlined"
        size="small"
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          setFirstName(e.target.value);
        }}
        error={blank}
        helperText={blank === true ? "No blank field allowed" : ""}
      />
      <TextField
        value={lastname}
        label="Last name"
        variant="outlined"
        size="small"
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          setLastName(e.target.value);
        }}
        error={blank}
        helperText={blank === true ? "No blank field allowed" : ""}
      />
      <TextField
        value={email}
        label="Email"
        variant="outlined"
        size="small"
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          setEmail(e.target.value);
        }}
        error={blank}
        helperText={blank === true ? "No blank field allowed" : ""}
      />

      <FormControl>
        <InputLabel id="rolelabel">Role</InputLabel>
        <Select
          labelId="rolelabel"
          value={role}
          onChange={(event: React.ChangeEvent<{ value: unknown }>) =>
            setRole(event.target.value as string)
          }
          label="Role"
        >
          <MenuItem value={"admin"}>admin</MenuItem>
          <MenuItem value={"user"}>user</MenuItem>
        </Select>
      </FormControl>

      <FormControlLabel
        control={
          <CheckBox checked={enabled} onChange={() => setEnabled(!enabled)} />
        }
        label="Enabled"
      />

      <Button
        variant="contained"
        color="primary"
        size="large"
        className={classes.button}
        onClick={() => {
          add();
        }}
        startIcon={<SaveIcon />}
      >
        Save
      </Button>
      <Button
        variant="contained"
        color="primary"
        size="large"
        className={classes.button}
        onClick={() => {
          closeDialog();
        }}
        startIcon={<CloseIcon />}
      >
        Close
      </Button>
    </FormControl>
  );
}
